package com.lyproject.emotionly;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity {

    private final int REQ_CODE_SPEECH_INPUT = 200;
    Button btnSendMessage, send;
    TextView txtSpeechInput, suggestionsText1;
    TextView currentMood, txtAnger, txtDisgust, txtFear, txtJoy, txtSadness, txtAnalytical, txtConfident, txtTentative, txtOpenness, txtConscientiousneous, txtExtraversion, txtAgreeableness, txtEmotionalRange;
    private static String MESSAGE;
    JSONArray jsonArray;
    JSONObject jsonObject;
    EditText input;
    int prevLength = 0;
    int curLength = 0;
    String isCorrect="";


    float max;
    String mood = "";


    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("http://hdluong-tone-analyzer.mybluemix.net");
            //mSocket = IO.socket("http://192.168.1.16:3000");
            // mSocket = IO.socket("http://10.1.14.190:3000");
        } catch (URISyntaxException e) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        imeManager.showInputMethodPicker();



        try {
            mSocket.connect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mSocket.on("result-json", onNewMessage);

        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);

        txtAnger = (TextView) findViewById(R.id.txtAnger);
        txtDisgust = (TextView) findViewById(R.id.txtDisgust);
        txtFear = (TextView) findViewById(R.id.txtFear);
        txtJoy = (TextView) findViewById(R.id.txtJoy);
        txtSadness = (TextView) findViewById(R.id.txtSadness);
        txtAnalytical = (TextView) findViewById(R.id.txtAnalytical);
        txtConfident = (TextView) findViewById(R.id.txtConfident);
        txtTentative = (TextView) findViewById(R.id.txtTentative);
        txtOpenness = (TextView) findViewById(R.id.txtOpenness);
        txtConscientiousneous = (TextView) findViewById(R.id.txtConscientiousneous);
        txtExtraversion = (TextView) findViewById(R.id.txtExtraversion);
        txtAgreeableness = (TextView) findViewById(R.id.txtAgreeableness);
        txtEmotionalRange = (TextView) findViewById(R.id.txtEmotionalRange);
        //   currentMood = (TextView) findViewById(R.id.currentMood);

        suggestionsText1 = (TextView) findViewById(R.id.suggestion1);


        //all visible gone
        txtAnalytical.setVisibility(View.GONE);
        txtConfident.setVisibility(View.GONE);
        txtTentative.setVisibility(View.GONE);
        txtOpenness.setVisibility(View.GONE);
        txtConscientiousneous.setVisibility(View.GONE);
        txtExtraversion.setVisibility(View.GONE);
        txtAgreeableness.setVisibility(View.GONE);
        txtEmotionalRange.setVisibility(View.GONE);


        input = (EditText) findViewById(R.id.inputText);
        send = (Button) findViewById(R.id.send);


        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        isCorrect="yes";
                        sendFeedback("https://emotionly.000webhostapp.com/API/insertfeedback.php");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        isCorrect="no";
                        sendFeedback("https://emotionly.000webhostapp.com/API/insertfeedback.php");
                        break;
                }
            }
        };


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Did Emotionly correctly detect your mood?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mSocket.emit("send-message", input.getText().toString());

       //

                builder.show();



            }
        });


        input.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Abstract Method of TextWatcher Interface.
            }

            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
                // Abstract Method of TextWatcher Interface.
                prevLength = s.length();

            }

            public void onTextChanged(CharSequence s,
                                      int start, int before, int count) {
                System.out.println(s);
                max=0;
                curLength = s.length();

                mSocket.emit("send-message", input.getText().toString().trim());


                if (!s.toString().isEmpty())
                    if (s.toString().charAt(s.length() - 1) == ' ') {

                        if(curLength-prevLength==1)
                        sendWords();


                        //if(curLength-prevLength==1)
                        //      sendWords();


                        //   mSocket.emit("send-message", input.getText().toString());


                    }


            }
        });


    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject root = (JSONObject) args[0];
                    JSONObject jsonFile;
                    try {
                        jsonFile = new JSONObject(root.getString("jsonFile"));

                        JSONObject document_tone = new JSONObject(jsonFile.getString("document_tone"));

                        JSONArray tone_categories = new JSONArray(document_tone.getString("tone_categories"));

                        for (int i = 0; i < tone_categories.length(); i++) {
                            JSONObject sub_category = tone_categories.getJSONObject(i);
                            JSONArray tones = new JSONArray(sub_category.getString("tones"));


                            for (int j = 0; j < tones.length(); j++) {
                                JSONObject sub_tone = tones.getJSONObject(j);
                                if (i == 0) {

                                /*    if (max < Float.parseFloat(sub_tone.getString("score"))) {
                                        max = Float.parseFloat(sub_tone.getString("score"));

                                        Log.d("max", String.valueOf(max));
                                    }*/


                                    switch (j) {
                                        case 0:
                                            txtAnger.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtDisgust.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtFear.setText(sub_tone.getString("score"));
                                            break;
                                        case 3:
                                            txtJoy.setText(sub_tone.getString("score"));
                                            break;
                                        case 4:
                                            txtSadness.setText(sub_tone.getString("score"));

                                            break;
                                    }
                                } else if (i == 1) {
                                    switch (j) {
                                        case 0:
                                            txtAnalytical.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtConfident.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtTentative.setText(sub_tone.getString("score"));
                                            break;
                                    }
                                } else {
                                    switch (j) {
                                        case 0:
                                            txtOpenness.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtConscientiousneous.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtExtraversion.setText(sub_tone.getString("score"));
                                            break;
                                        case 3:
                                            txtAgreeableness.setText(sub_tone.getString("score"));
                                            break;
                                        case 4:
                                            txtEmotionalRange.setText(sub_tone.getString("score"));
                                            break;
                                    }
                                }
                            }

                        }



                        getMood();

                        //COMMENTED TEMPORARILY
                        //JSONObject sub_category = tone_categories.getJSONObject(0);
                      //  JSONArray tones = new JSONArray(sub_category.getString("tones"));

                      /*  for (int j = 0; j < tones.length(); j++) {
                            JSONObject sub_tone = tones.getJSONObject(j);


                            Log.d("maxval",max+"");
                            Log.d("scoreval",Float.parseFloat(sub_tone.getString("score"))+"");

                          if (max == Float.parseFloat(sub_tone.getString("score"))) {

                                System.out.println(sub_tone);
                                System.out.println(max);


                                if (j == 0) {
                                    mood = "anger";
                                } else if (j == 1) {
                                    mood = "disgust";
                                } else if (j == 2) {
                                    mood = "fear";

                                } else if (j == 3) {
                                    mood = "joy";
                                } else if (j == 4) {
                                    mood = "sadness";
                                }

                               // max = 0;
                                Log.d("mood", mood);

                            }


                        }*/



                        //String tone_categories = document_tone.getString("tone_categories");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };


    public boolean OnCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void getMood(){
        Log.d("getMood-MAX", max+"");



try {
    getMax();


    if (max == Float.parseFloat(txtJoy.getText().toString())) {
        mood = "joy";
    } else if (max == Float.parseFloat(txtSadness.getText().toString())) {
        mood = "sad";
    } else if (max == Float.parseFloat(txtFear.getText().toString())) {
        mood = "fear";
    } else if (max == Float.parseFloat(txtDisgust.getText().toString())) {
        mood = "disgust";
    } else if (max == Float.parseFloat(txtAnger.getText().toString())) {
        mood = "anger";
    }


    Log.d("MOOD", mood);

}
catch(Exception e){
    Log.d("Exception", "EXCEPTION CAUGHT");
}
    }


    public void getMax(){
        if(max<Float.parseFloat(txtJoy.getText().toString())){
            max = Float.parseFloat(txtJoy.getText().toString());
        }

        if(max<Float.parseFloat(txtSadness.getText().toString())){
            max = Float.parseFloat(txtSadness.getText().toString());
        }

        if(max<Float.parseFloat(txtDisgust.getText().toString())){
            max = Float.parseFloat(txtDisgust.getText().toString());
        }

        if(max<Float.parseFloat(txtFear.getText().toString())){
            max = Float.parseFloat(txtFear.getText().toString());
        }

        if(max<Float.parseFloat(txtAnger.getText().toString())){
            max = Float.parseFloat(txtAnger.getText().toString());
        }


          Log.d("INSIDE GETMAX", max+"");
    }

    public String getMood(String someting){
        Log.d("getMood-MAX", max+"");






        try {

            getMax();

            if (max == Float.parseFloat(txtJoy.getText().toString())) {
                mood = "joy";
            } else if (max == Float.parseFloat(txtSadness.getText().toString())) {
                mood = "sad";
            } else if (max == Float.parseFloat(txtFear.getText().toString())) {
                mood = "fear";
            } else if (max == Float.parseFloat(txtDisgust.getText().toString())) {
                mood = "disgust";
            } else if (max == Float.parseFloat(txtAnger.getText().toString())) {
                mood = "anger";
            } else {

            }


            Log.d("MOOD", mood);

        }
        catch(Exception e){
            Log.d("Exception", "EXCEPTION CAUGHT");
        }

        return  mood;
    }




    public void sendWords() {
        final String URL = "http://192.168.1.10:8080/summary/";

        getMood();
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest postRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response

                        Log.d("RESPONSE", response);

                        String resp = response.replace(",", "<br>");


                        suggestionsText1.setText(Html.fromHtml(resp));


                        // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                String wrds[] = input.getText().toString().trim().split(" ");

                params.put("message", wrds[wrds.length - 1]);
                params.put("mood", getMood(""));

                Log.d("mood before sending", getMood(""));


                return params;
            }
        };
        queue.add(postRequest);
    }



    public void sendFeedback(String URL) {

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest postRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response



                        if(response.contains("true"))
                         Toast.makeText(getApplicationContext(), "Feedback saved", Toast.LENGTH_SHORT).show();
                        else if(response.contains("false"))
                            Toast.makeText(getApplicationContext(), "Could not save feedback", Toast.LENGTH_SHORT).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                String wrds[] = input.getText().toString().trim().split(" ");

                params.put("text", input.getText().toString().trim());
                params.put("mood", getMood(""));
                params.put("correct", isCorrect);



                return params;
            }
        };
        queue.add(postRequest);
    }


}