package com.lyproject.emocollect;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity {

    private final int REQ_CODE_SPEECH_INPUT = 200;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
    Timestamp timeStampVal;
    static AlertDialog mDialog, userInfoDialog;
    EditText personAge, personName;
    Button btnSendMessage, send;
    Button chooseMood;
    TextView txtSpeechInput, currentMoodTitle;
    TextView currentMood, txtAnger, txtDisgust, txtFear, txtJoy, txtSadness, txtAnalytical, txtConfident, txtTentative, txtOpenness, txtConscientiousneous, txtExtraversion, txtAgreeableness, txtEmotionalRange;
    private static String MESSAGE;
    EditText input;
    float max;
    String mood="";
    String myMood="";
    String[] moodList;
    int spaceCount=0;
    int backSpaceCount=0;
    String name="";
    String age="";
    String timestamp="";
    int prevLength=0;
    int curLength=0;

    long startTime,endTime;
    long speed;

    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("http://hdluong-tone-analyzer.mybluemix.net");
            //mSocket = IO.socket("http://192.168.1.16:3000");
            // mSocket = IO.socket("http://10.1.14.190:3000");
        } catch (URISyntaxException e) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        imeManager.showInputMethodPicker();

        try {
            mSocket.connect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mSocket.on("result-json", onNewMessage);

        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);

        txtAnger = (TextView) findViewById(R.id.txtAnger);
        txtDisgust = (TextView) findViewById(R.id.txtDisgust);
        txtFear = (TextView) findViewById(R.id.txtFear);
        txtJoy = (TextView) findViewById(R.id.txtJoy);
        txtSadness = (TextView) findViewById(R.id.txtSadness);
        txtAnalytical = (TextView) findViewById(R.id.txtAnalytical);
        txtConfident = (TextView) findViewById(R.id.txtConfident);
        txtTentative = (TextView) findViewById(R.id.txtTentative);
        txtOpenness = (TextView) findViewById(R.id.txtOpenness);
        txtConscientiousneous = (TextView) findViewById(R.id.txtConscientiousneous);
        txtExtraversion = (TextView) findViewById(R.id.txtExtraversion);
        txtAgreeableness = (TextView) findViewById(R.id.txtAgreeableness);
        txtEmotionalRange = (TextView) findViewById(R.id.txtEmotionalRange);

        currentMoodTitle = (TextView) findViewById(R.id.currentMoodTitle);

        //   currentMood = (TextView) findViewById(R.id.currentMood);

        input = (EditText) findViewById(R.id.inputText);
        send = (Button) findViewById(R.id.send);
        chooseMood = (Button) findViewById(R.id.chooseMood);







        moodList = new String[]{"Anger","Disgust","Fear","Joy","Sadness"};



        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.user_info, null);


        personAge = (EditText) promptsView.findViewById(R.id.person_age);
        personName = (EditText) promptsView.findViewById(R.id.person_name);






        final AlertDialog.Builder userInfo = new AlertDialog.Builder(MainActivity.this);
        userInfo.setTitle("Enter your Name & Age");
        userInfo.setView(promptsView);
        userInfo.setCancelable(false).setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Log.d("details", personAge.getText().toString()+" "+personName.getText().toString());





            }
        });

        userInfo.setCancelable(false).setNegativeButton("exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
            }
        });





        userInfoDialog = userInfo.create();


            userInfoDialog.show();

        Button theButton = userInfoDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        theButton.setOnClickListener(new CustomListener(userInfoDialog));




        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        mBuilder.setTitle("Select your mood");
        mBuilder.setSingleChoiceItems(moodList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                myMood = moodList[i];

                Log.d("myMood",myMood);

                currentMoodTitle.setText(myMood);

                dialogInterface.dismiss();
            }
        });


       mDialog = mBuilder.create();
    //   mDialog.show();




        chooseMood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.show();

            }
        });




        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  mSocket.emit("send-message", input.getText().toString());

                endTime = System.currentTimeMillis();




                if(input.getText().toString().trim().equals("")){
                      Toast.makeText(getApplicationContext(), "Input field cannot be empty", Toast.LENGTH_SHORT).show();

                }

                else {


                    String[] tempWordCounter = input.getText().toString().split(" ");

                    Log.d("debug"," "+tempWordCounter.length+" "+endTime+" "+startTime);


                    Log.d("minutes",(endTime - startTime)/1000.0 +"");

                    speed = (long) (tempWordCounter.length*0.1*60 / ((endTime - startTime)*0.1 / 1000));


                    Log.d("SPEED" , speed+"");

                    timeStampVal = new Timestamp(System.currentTimeMillis());

                    timestamp = sdf.format(timeStampVal);
                    Log.d("TIMESTAMP",sdf.format(timeStampVal));



                    insert("https://emotionly.000webhostapp.com/API/insert.php");




                }
            }
        });



        input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Log.d("FOCUS","FOCUSSED");

                    startTime = System.currentTimeMillis();

                }
            }
        });



        input.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Abstract Method of TextWatcher Interface.
            }

            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
                // Abstract Method of TextWatcher Interface.
                      prevLength  = s.length();

            }

            public void onTextChanged(CharSequence s,
                                      int start, int before, int count) {
                System.out.println(s);


                curLength = s.length();


             //   mSocket.emit("send-message", input.getText().toString());

                if(curLength-prevLength==1)
                if (!s.toString().isEmpty())
                    if (s.toString().charAt(s.length() - 1) == ' ') {
                       // mSocket.emit("send-message", input.getText().toString());
                             spaceCount = spaceCount+1;
                             Log.d("KEYS", "SPACE "+spaceCount);
                     //   Toast.makeText(getApplicationContext(), "SPACE "+spaceCount, Toast.LENGTH_SHORT).show();


                    }
                if(prevLength-curLength==1){
                    backSpaceCount = backSpaceCount+1;
                    Log.d("KEYS", "BACKSPACE "+backSpaceCount);

                   // Toast.makeText(getApplicationContext(), "BACKSPACE "+backSpaceCount, Toast.LENGTH_SHORT).show();

                }





            }
        });


        btnSendMessage = (Button) findViewById(R.id.btnSendMessage);
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });

    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject root = (JSONObject) args[0];
                    JSONObject jsonFile;
                    try {
                        jsonFile = new JSONObject(root.getString("jsonFile"));

                        JSONObject document_tone = new JSONObject(jsonFile.getString("document_tone"));

                        JSONArray tone_categories = new JSONArray(document_tone.getString("tone_categories"));

                        for (int i = 0; i < tone_categories.length(); i++) {
                            JSONObject sub_category = tone_categories.getJSONObject(i);
                            JSONArray tones = new JSONArray(sub_category.getString("tones"));


                            for (int j = 0; j < tones.length(); j++) {
                                JSONObject sub_tone = tones.getJSONObject(j);
                                if (i == 0) {

                                  if(max< Float.parseFloat(sub_tone.getString("score"))){
                                      max=Float.parseFloat(sub_tone.getString("score"));

                                      Log.d("max",String.valueOf(max));
                                    }



                                    switch (j) {
                                        case 0:
                                            txtAnger.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtDisgust.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtFear.setText(sub_tone.getString("score"));
                                            break;
                                        case 3:
                                            txtJoy.setText(sub_tone.getString("score"));
                                            break;
                                        case 4:
                                            txtSadness.setText(sub_tone.getString("score"));

                                            break;
                                    }
                                } else if (i == 1) {
                                    switch (j) {
                                        case 0:
                                            txtAnalytical.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtConfident.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtTentative.setText(sub_tone.getString("score"));
                                            break;
                                    }
                                } else {
                                    switch (j) {
                                        case 0:
                                            txtOpenness.setText(sub_tone.getString("score"));
                                            break;
                                        case 1:
                                            txtConscientiousneous.setText(sub_tone.getString("score"));
                                            break;
                                        case 2:
                                            txtExtraversion.setText(sub_tone.getString("score"));
                                            break;
                                        case 3:
                                            txtAgreeableness.setText(sub_tone.getString("score"));
                                            break;
                                        case 4:
                                            txtEmotionalRange.setText(sub_tone.getString("score"));
                                            break;
                                    }
                                }
                            }

                        }

                        JSONObject sub_category = tone_categories.getJSONObject(0);
                        JSONArray tones = new JSONArray(sub_category.getString("tones"));

                        for (int j = 0; j < tones.length(); j++) {
                            JSONObject sub_tone = tones.getJSONObject(j);



                                    if(max==Float.parseFloat(sub_tone.getString("score"))){

                                        System.out.println(sub_tone);
                                        System.out.println(max);


                                        if(j==0){
                                            mood="anger";
                                        }
                                        else if(j==1){
                                            mood="disgust";
                                        }
                                        else if(j==2){
                                            mood="fear";

                                        }
                                        else if(j==3){
                                            mood="joy";
                                        }
                                        else if(j==4){
                                            mood="sadness";
                                        }

                                        max=0;

                                    }




                            }

                        Log.d("mood",mood);




                        //String tone_categories = document_tone.getString("tone_categories");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(resultCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));
                    MESSAGE = result.get(0);
                    mSocket.emit("send-message", MESSAGE);
                }
                break;
            }
        }
    }

    public boolean OnCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void insert(String url) {


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String URL = url;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {

                        if(response.contains("true")) {
                            // response
                            Log.d("Response", response);
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Successfully Inserted",
                                    Toast.LENGTH_SHORT);

                            toast.show();


                            input.clearFocus();
                            input.setText("");


                            backSpaceCount = 0;
                            spaceCount = 0;
                            speed = 0;

                        }
                        else{

                            Log.d("err", response);

                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Failed to Insert",
                                    Toast.LENGTH_SHORT);

                            toast.show();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Could not Insert",
                                Toast.LENGTH_SHORT);

                        toast.show();




                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("text", input.getText().toString() );
                //params.put("anger", txtAnger.getText().toString());
               // params.put("joy", txtJoy.getText().toString());
               // params.put("disgust", txtDisgust.getText().toString());
               // params.put("sadness", txtSadness.getText().toString());
              //  params.put("fear", txtFear.getText().toString());
                params.put("backspace", backSpaceCount+"");
                params.put("space", spaceCount+"");
                params.put("speed", speed+"");
                params.put("mood", myMood);
                params.put("name", name);
                params.put("age", age);
                params.put("timestamp", timestamp);

              //  Log.d("testing", age+" "+name+" "+timestamp);



                return params;
            }
        };

        requestQueue.add(postRequest);
    }


    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        public CustomListener(Dialog dialog) {
            this.dialog = dialog;
        }
        @Override
        public void onClick(View v) {
            // put your code here

            if(personAge.getText().toString().equals("") || personName.getText().toString().equals("")){
                Toast.makeText(MainActivity.this, "Input fields cannot be empty", Toast.LENGTH_SHORT).show();

            }else{
                age = personAge.getText().toString();
                name = personName.getText().toString();
                


                dialog.dismiss();
                MainActivity.mDialog.show();
            }
        }
    }


}
